<?php
	
function awesome_script_enqueue() {
	
	wp_enqueue_style('customstyle', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), '1.0.0', 'all');
		wp_enqueue_script('customq', get_template_directory_uri() . '/assets/css/bootstrap-theme.css', array(), '1.0.0', true);

	wp_enqueue_style('style', get_template_directory_uri() . '/style.css', array(), '1.0.0', 'all');

	wp_enqueue_script('custom', get_template_directory_uri() . '/assets/js/bootstrap.js', array(), '1.0.0', true);

		wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/script.js', array(), '1.0.0', true);

}

add_action( 'wp_enqueue_scripts', 'awesome_script_enqueue');