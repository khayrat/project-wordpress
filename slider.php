
<div id="myCarousels" class="carousel slide" data-ride="carousel" data-interval="false" style="margin-top: 49px;">
			<!-- Indicators -->
			<ol class="carousel-indicators hidden-xs">
			  	<li data-target="#myCarousels" data-slide-to="0" class="active"></li>
			    <li data-target="#myCarousels" data-slide-to="1"></li>
			    <li data-target="#myCarousels" data-slide-to="2"></li>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
			    <div class="item active">
			    	<img src="<?php echo get_theme_file_uri('assets/images/01.jpg'); ?>" class="img-responsive" alt="Second slide">
			    	<!-- Static Header -->
                    <div class="header-text text-center ">
                       <div class="main_title ">
			                <h2>Paris <span>Top</span> Tours</h2>
			                <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>
	                   </div>  
	                        <div class="small">
                                <a class="btn btn-lg btn-primary site-btn"  data-toggle="modal" data-target="#project1" >Learn More</a>
                            </div>
                    </div><!-- /header-text -->
			    </div>
			    <div class="item">
			    	<img src="<?php echo get_theme_file_uri('assets/images/02.jpg'); ?>" class="img-responsive" alt="Second slide">
			    	<!-- Static Header -->
                    <div class="header-text text-center ">
                       <div class="main_title ">
			                <h2>Paris <span>Top</span> Tours</h2>
			                <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>
	                   </div>  
	               <div class="small">
                                <a class="btn btn-lg btn-primary site-btn" data-toggle="modal" data-target="#project2">Learn More</a>
                               
                            </div>
	            
                    </div><!-- /header-text -->
			    </div>
			   <div class="item">
			    	<img src="<?php echo get_theme_file_uri('assets/images/03.jpg'); ?>" class="img-responsive" alt="Second slide">
			    	<!-- Static Header -->
                    <div class="header-text text-center ">
                       <div class="main_title ">
			                <h2>Paris <span>Top</span> Tours</h2>
			                <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>
	                   </div>
	                   <div class="small">
                                <a class="btn btn-lg btn-primary site-btn" data-toggle="modal" data-target="#project3">Learn More</a>
                                
                            </div>
	            
                    </div><!-- /header-text -->
			    </div>
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#myCarousels" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#myCarousels" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
</div><!-- /carousel -->
