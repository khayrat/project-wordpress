<?php
/**
**Template Name: Home Page
**/


 get_header(); ?>
 <section class="navbar-head">
	 <nav class="navbar navbar-default  navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#alignment-example" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Your Brand</a>
			</div>
			<div class="collapse navbar-collapse" id="alignment-example">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Link 1 <span class="sr-only">(current)</span></a></li>
					<li><a href="#">Link 2</a></li>
					<li><a href="#">Link 3</a></li>
				</ul>
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control">
					</div>
					<button type="submit" class="btn btn-default">Search</button>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">Link 4</a></li>
				</ul>
			</div>
		</div>
	</nav>
</section>
<section id="slider">
	<?php include 'slider.php'; ?>
	<?php include 'modal.php'; ?>

</section>
<section id="text-section">
	<div class="container-fluid">
		<div class="row ">
			<div class="col-xs-6 col-md-3 section-text">
				<p class="text-center">Text 1</p>
			</div>
			<div class="col-xs-6  col-md-3 section-text">
				<p class="text-center ">Text 2</p>
			</div>
			<div class="col-xs-6  col-md-3 section-text">
				<p class="text-center ">Text 3</p>
			</div>
			<div class="col-xs-6 col-md-3 section-text">
				<p class="text-center ">Text 4</p>
			</div>
			<div class="col-xs-12 border"></div>
		</div>
	</div>
</section>
<section id="image">
	<div class="container-fluid container-scection">
		<div class="row ">
			<div class="col-xs-6  col-md-3 section-text bottom-mobile">
  				<img src="<?php echo get_theme_file_uri('assets/images/01.jpg'); ?>" alt="Norway" style="width:100%;">
				<p class="centered">Text 1</p>
			</div>
			<div class="col-xs-6 col-md-3 section-text bottom-mobile">
  				<img src="<?php echo get_theme_file_uri('assets/images/01.jpg'); ?>" alt="Norway" style="width:100%;">
				<p class="centered ">Text 2</p>
			</div>
			<div class="col-xs-6 col-md-3 section-text bottom-mobile">
  				<img src="<?php echo get_theme_file_uri('assets/images/01.jpg'); ?>" alt="Norway" style="width:100%;">
				<p class="centered">Text 3</p>
			</div>
			<div class="col-xs-6 col-md-3 section-text bottom-mobile">
  				<img src="<?php echo get_theme_file_uri('assets/images/01.jpg'); ?>" alt="Norway" style="width:100%;">
				<p class="centered">Text 4</p>
			</div>
		</div>
	</div>
</section>
<section id="protofolio">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<p class="text-left title">Title page</p>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="text-center">Portofolio image</h1>
			</div>
		</div>
	</div>
	<div class="container-fluid full-width-image "></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<p class="text-center description">Portofolio description</p>
			</div>
		</div>
	</div>

</section>
<!-- Modal -->

<?php get_footer(); ?>
