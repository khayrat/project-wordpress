<div class="modal fade" id="project1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">project 1</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-6">
              <img src="<?php echo get_theme_file_uri('assets/images/01.jpg'); ?>" class="img-responsive" alt="Second slide">

            </div>
            <div class="col-xs-6">
              Lorem ipsum dolor sit amet, amet augue integer, tempor a qui montes tellus, donec ornare, amet ante faucibus vivamus euismod quam. Tincidunt ipsum in, ullamcorper ut faucibus placerat pede justo, ullamcorper auctor nec consequat enim ad donec. Tempor varius sapien odio venenatis sollicitudin nec, nec ligula sed porta morbi adipiscing, et fringilla eget etiam neque nonummy faucibus. Venenatis dis tristique venenatis viverra pede senectus, sed gravida dui quis nulla volutpat. Amet lectus velit vel amet nunc, cum vel a lobortis felis dignissim, eget mus odio wisi, sit dui. Turpis aut torquent quis ullamcorper fringilla nam.
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="project2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">project 2</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-6">
              <img src="<?php echo get_theme_file_uri('assets/images/02.jpg'); ?>" class="img-responsive" alt="Second slide">

            </div>
            <div class="col-xs-6">
              Lorem ipsum dolor sit amet, amet augue integer, tempor a qui montes tellus, donec ornare, amet ante faucibus vivamus euismod quam. Tincidunt ipsum in, ullamcorper ut faucibus placerat pede justo, ullamcorper auctor nec consequat enim ad donec. Tempor varius sapien odio venenatis sollicitudin nec, nec ligula sed porta morbi adipiscing, et fringilla eget etiam neque nonummy faucibus. Venenatis dis tristique venenatis viverra pede senectus, sed gravida dui quis nulla volutpat. Amet lectus velit vel amet nunc, cum vel a lobortis felis dignissim, eget mus odio wisi, sit dui. Turpis aut torquent quis ullamcorper fringilla nam.
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<div class="modal fade" id="project3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">project 3</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-6">
              <img src="<?php echo get_theme_file_uri('assets/images/03.jpg'); ?>" class="img-responsive" alt="Second slide">

            </div>
            <div class="col-xs-6">
              Lorem ipsum dolor sit amet, amet augue integer, tempor a qui montes tellus, donec ornare, amet ante faucibus vivamus euismod quam. Tincidunt ipsum in, ullamcorper ut faucibus placerat pede justo, ullamcorper auctor nec consequat enim ad donec. Tempor varius sapien odio venenatis sollicitudin nec, nec ligula sed porta morbi adipiscing, et fringilla eget etiam neque nonummy faucibus. Venenatis dis tristique venenatis viverra pede senectus, sed gravida dui quis nulla volutpat. Amet lectus velit vel amet nunc, cum vel a lobortis felis dignissim, eget mus odio wisi, sit dui. Turpis aut torquent quis ullamcorper fringilla nam.
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>